import state from './state'
import * as getters from './getters'
import * as mutations from './mutations'
import * as actions from './actions'

export default {
  namespaced: true,
  state, /* holds the data on the Vue specifically this is the store */
  getters, /* shares to the whole Vue Project */
  mutations, /* PUT */
  actions /* GET */
}
