
const routes = [
  {
    path: '/',
    component: () => import('layouts/MyLayout.vue'),
    children: [
      { path: '', name: 'login', component: () => import('pages/auth.vue') },
      { path: 'signup', name: 'signup', component: () => import('pages/auth.vue') },
      { path: 'forgetPwd', name: 'forgetPwd', component: () => import('pages/auth.vue') },
      { path: 'index', name: 'index', component: () => import('pages/Index.vue') },
    ]
  }
]

// Always leave this as last one
if (process.env.MODE !== 'ssr') {
  routes.push({
    path: '*',
    component: () => import('pages/Error404.vue')
  })
}

export default routes
